# Curso Mixed Integer Linear Programming (MILP)

Todo el *código fuente* para una actividad acreditable de 1 crédito ECTS sobre **Programación Lineal con Variables Mixtas**. Impartida en la **ETS de Ingenieros Navales de la UPM en Madrid**.

 - temario detallado, temporalización y lecturas recomendadas para cada semana
 - resúmenes de teoría (dos *cheatsheet* en *inglés*)
 - hojas de ejercicios (en *castellano*)
 - cuadernos `jupyter` con prácticas en `python` (en *castellano*)

El contenido se ha probado de forma presencial y telemática, pero no es un MOOC y los alumnos necesitan ayuda de un profesor.

## Install / Build

 - Las cheatsheet, hojas de ejercicios y varios de los exámenes están disponibles en `pdf`. Para poder editarlas es suficiente instalar `LaTeX`.
 - Parte de los exámenes fueron generados con [auto-multiple-choice](https://auto-multiple-choice.net/), y es necesario instalar este programa para poder editarlos.
 - Los cuadernos `jupyter` usan `python3` y algunas librerías. Se pueden instalar todas las librerías necesarias con un comando:

```python
python3 -m pip install numpy scipy matplotlib ipython jupyter pandas sympy optlang nose
```

## Temario

Desde el principio hasta problemas NP-duros. El énfasis es en la modelización, no se habla nada de simplex ni otros algoritmos.

Pensada para cinco clases de 100 minutos cada una aprox, y trabajo independiente fuera del aula.

[Temario detallado](TEMARIO.md)

## Cheatsheet

`milp-cheatsheet.pdf`, resumen de comandos `python`, `numpy` y `matplotlib`, y un resumen de programación lineal con variables mixtas (MILP).

## Hoja de ejercicios, en *castellano*

Programación lineal con variables continuas y enteras.

## Cuadernos `jupyter`

  - [`Intro_LP`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Intro_LP.ipynb): introducción a la programación lineal con variables continuas
  - [`Intro_MILP`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Intro_MILP.ipynb): introducción a la programación lineal con variables mixtas, continuas y enteras  ("Mixed Integer Linear Programming")
  - [`Dieta`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Dieta.ipynb): aplicación al diseño de la dieta óptima
  - [`Transport`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/Transport.ipynb): aplicación al problema de transporte
  - [`knapsack`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/knapsack.ipynb): aplicación al problema de mochila
  - [`sudoku`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/sudoku.ipynb): aplicación a la resolución de Sudokus (problemas similares a confeccionar horarios y planes de trabajo)
  - [`viajante`](https://nbviewer.jupyter.org/url/framagit.org/pang/optimizacion-y-estadistica/-/raw/main/cuadernos/MILP/viajante.ipynb): aplicación al problema del viajante ("Travelling salesman").
    + Motiva el enfoque de Dantzig–Fulkerson–Johnson, aunque no la desarrolla
    + Implementa la formulación de Miller–Tucker–Zemlin
    + Aplica lo anterior a viajar por capitales españolas. Buena ocasión para introducir el uso del `timeout` ;-)

## Licencia

Todo el contenido original se distribuye con estas tres licencias, a elección del usuario:

 - [`GFDL`](http://www.gnu.org/copyleft/fdl.html)
 - [`Creative Commons Attribution-Share Alike 3.0 License`](http://creativecommons.org/licenses/by/3.0/deed.es_ES)
 - [`cc-by-nc`](http://creativecommons.org/licenses/by-nc/3.0/deed.es_ES)

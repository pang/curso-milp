## Semana 1:  Problemas LP

En esta primera semana estudiamos los problemas LP en dos dimensiones, a mano y usando python con la librería optlang.

En el servidor jupyter tenéis el cuaderno [Intro_LP](cuadernos/Intro_LP.ipynb) con ejemplos de uso de optlang, la hoja de ejercicios y la [chuleta MILP](cheatsheet/milp_cheatsheet.pdf).

Para la semana siguiente intentad los ejercicios del cuaderno Intro_LP y los [ejercicios de la hoja](ejercicios/ejer_MILP.pdf) hasta el 6.

Si os cuesta modelizar problemas a partir del enunciado, en internet es fácil encontrar vídeos de ejercicios resueltos:

https://www.youtube.com/results?search_query=david+calle+programacion+lineal

Hay vídeos grabados de esta clase:

* vídeo: Introducción a LP y MILP: https://drive.upm.es/s/3UJbOuwHuFXfB30
* vídeo: Ejercicios MILP 2D con papel: https://drive.upm.es/s/H2mGh8G3tSVczwD
* vídeo: Ejercicios con optlang: https://drive.upm.es/s/FMVzKveh9cgRUaN


## Semana 2: Intro_MILP y ejercicios

En la segunda semana hacemos ejercicios de modelización sencillos, estudiamos la clasificación de los problemas MILP, y estudiamos el problema de mochila como MILP, contrastando con la versión relajada.
* ejercicios de la hoja hasta el 8
* Cuadernos [Intro_MILP](cuadernos/Intro_MILP.ipynb), [knapsack](cuadernos/knapsack.ipynb)

Hay vídeos grabados de esta clase:

* vídeo: Ejercicio 6 hoja de ejercicios: https://drive.upm.es/s/3EddfMcI99Yi4j0
* vídeo: Ejercicio transporte wikipedia: https://drive.upm.es/s/AnYfXRoF2v9Aj1V
* vídeo: mochila: https://drive.upm.es/s/7LFDdH137o2NP9T

## Semana 3: Dieta, transporte

Esta semana aprendemos a introducir problemas con muchas variables, y practicamos a plantear variantes de un problema, cambiando el objetivo o las restricciones.

* Cuadernos [Dieta](cuadernos/Dieta.ipynb), [transporte](cuadernos/Transport.ipynb)

Hay vídeos grabados de esta parte de esta clase:

* vídeo: dieta: https://drive.upm.es/s/JYyNI6JsDtXt1Hp

## Semana 4: Sudoku

Planteamos el problema de resolver sudokus como problema MILP, para practicar el modelado con variables de decisión 0/1. Estudiamos también un plantamiento relajado del problema del sudoku, con una región factible más grande, moviendo parte de la carga del problema de las restricciones al objetivo. Esta técnica es útil cuando planteamos problemas inviables, y queremos pensar en planteamientos similares que se parezcan lo más posible al problema original.

* [sudoku](cuadernos/sudoku.ipynb)
* alguno de la [carpeta puzzles](cuadernos/puzzles)

Hay vídeos grabados de esta parte de esta clase:

* vídeo: sudoku: https://drive.upm.es/s/FcUcI5JPBvjyrP7

## Semana 5: NP-hard

Estudiamos las clases de complejidad P, NP, NP-hard y NP-complete. Reflexionamos sobre el hecho de que resolver problemas LP está en la clase P, pero resolver problemas MILP está en la clase NP-hard.

* Vídeo sobre la diferencia entre P y NP de Eduardo Sáenz de Cabezón: https://www.youtube.com/watch?v=UR2oDYZ-Sao
* Vídeo sobre las clases NP-hard, NP-complete, y BQP (asociada a ordenadores cuánticos): https://www.youtube.com/watch?v=KCFCcRmJAU8

Cuadernos

* [viajante](cuadernos/viajante.ipynb)
* [MILP_nonconvex](cuadernos/MILP_nonconvex.ipynb), para tratar con regiones no convexas o con funciones objetivo no lineales

Recapitulamos:

> Cuando un problema de optimización (horarios, logística, operaciones...) contiene subproblemas "similares" a un sudoku, un problema de mochila, un problema de viajante... es casi seguro que será un problema NP-duro, y no habrá ningún algoritmo obvio para resolverlo de forma eficiente. Plantearlo como un problema MILP con variables de decisión binarias y reales nos permitirá resolverlo en muchos casos de interés, sin necesidad de tener que pensar e implementar un algoritmo.
